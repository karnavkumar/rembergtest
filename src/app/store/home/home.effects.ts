import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { map, switchMap } from 'rxjs/operators';
import { RestApiService } from 'src/app/shared/services/rest-api.service';
import { AddSelectedAssetsSuccess, AssetsActionTypes, LoadAssetsSuccess, SelectedLoadAssetsSuccess } from './home.actions';

@Injectable()
export class HomeEffects {
  constructor(
    private actions$: Actions,
    private resetApi: RestApiService,
    private store: Store<any>
  ) { }

  @Effect()
  loadCustomers$ = this.actions$.pipe(
    ofType(AssetsActionTypes.LoadAssets),
    switchMap((action: any) => {
      return this.resetApi.getDataTableList(action.payload).pipe(
        map((data) => {
          return new LoadAssetsSuccess(data);
        })
      );
    })
  );

  @Effect()
  loadSelectedAssets$ = this.actions$.pipe(
    ofType(AssetsActionTypes.SelectedLoadAssets),
    switchMap((action: any) => {
      return this.resetApi.getSelectedDataTableList().pipe(
        map((data) => {
          return new SelectedLoadAssetsSuccess(data);
        })
      );
    })
  );

  @Effect()
  AddSelectedAssets$ = this.actions$.pipe(
    ofType(AssetsActionTypes.AddSelectedAssets),
    switchMap((action: any) => {
      return this.resetApi.addSelectedDataTableList(action.payload).pipe(
        map((data) => {
          return new AddSelectedAssetsSuccess(data);
        })
      );
    })
  );
}
