import { Action } from '@ngrx/store';

export enum AssetsActionTypes {
  LoadAssets = '[Assets] Load Assets',
  LoadAssetsSuccess = '[Assets] Assets Load Success',
  LoadAssetsFail = '[Assets] Assets Load Fail',
  SelectedLoadAssets = '[Assets] Selected Load Assets',
  SelectedLoadAssetsSuccess = '[Assets] Selected Assets Load Success',
  SelectedLoadAssetsFail = '[Assets] Selected Assets Load Fail',
  AddSelectedAssets = '[Assets] Add Selected Assets',
  AddSelectedAssetsSuccess = '[Assets] Add Selected Assets Load Success',
  AddSelectedAssetsFail = '[Assets] Add Selected Assets Load Fail',
}

export class LoadAssets implements Action {
  readonly type = AssetsActionTypes.LoadAssets;
  constructor(public payload: any) { }
}

export class LoadAssetsSuccess implements Action {
  readonly type = AssetsActionTypes.LoadAssetsSuccess;
  constructor(public payload: any) { }
}

export class LoadAssetsFail implements Action {
  readonly type = AssetsActionTypes.LoadAssetsFail;
  constructor(public payload: any) { }
}

export class SelectedLoadAssets implements Action {
  readonly type = AssetsActionTypes.SelectedLoadAssets;
  constructor(public payload: any) { }
}

export class SelectedLoadAssetsSuccess implements Action {
  readonly type = AssetsActionTypes.SelectedLoadAssetsSuccess;
  constructor(public payload: any) { }
}

export class SelectedLoadAssetsFail implements Action {
  readonly type = AssetsActionTypes.SelectedLoadAssetsFail;
  constructor(public payload: any) { }
}

export class AddSelectedAssets implements Action {
  readonly type = AssetsActionTypes.AddSelectedAssets;
  constructor(public payload: any) { }
}

export class AddSelectedAssetsSuccess implements Action {
  readonly type = AssetsActionTypes.AddSelectedAssetsSuccess;
  constructor(public payload: any) { }
}

export class AddSelectedAssetsFail implements Action {
  readonly type = AssetsActionTypes.AddSelectedAssetsFail;
  constructor(public payload: any) { }
}

export type AssetsAction = LoadAssets | LoadAssetsSuccess | LoadAssetsFail | SelectedLoadAssets | SelectedLoadAssetsSuccess | SelectedLoadAssetsFail | AddSelectedAssets | AddSelectedAssetsSuccess | AddSelectedAssetsFail;

export const fromCustomersActions = {
  LoadAssets,
  LoadAssetsSuccess,
  LoadAssetsFail,
  SelectedLoadAssets,
  SelectedLoadAssetsSuccess,
  SelectedLoadAssetsFail,
  AddSelectedAssets,
  AddSelectedAssetsSuccess,
  AddSelectedAssetsFail
};
