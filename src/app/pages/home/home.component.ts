import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { RestApiService } from '../../shared/services/rest-api.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import {
  LoadAssets,
  SelectedLoadAssets,
  AddSelectedAssets,
} from 'src/app/store/home/home.actions';
import { assetsQuery } from 'src/app/store/home/home.selector';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = [
    'select',
    'image',
    'name',
    'company_name',
    'owner',
  ];
  dataSource = new MatTableDataSource<any>();
  allSelectedIdList = new SelectionModel<any>(true, []);
  selection = new SelectionModel<any>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageSizeOptions = [5, 10, 20];
  length = 0;
  pageSize = 10;
  pageIndex = 0;
  isLoading = true;
  searchQuery: any = '';
  isSelectAll = false;
  destroy$: Subject<boolean> = new Subject<boolean>();
  assets$: Observable<any>;
  selectedAssets$: Observable<any>;
  addSelectedAssetsResponse$: Observable<any>;
  cacheIndex: any = 0;

  constructor(
    public restApi: RestApiService,
    private _snackBar: MatSnackBar,
    private readonly store: Store
  ) {}

  ngOnInit() {
    this.store.dispatch(
      new SelectedLoadAssets({
        pageSize: 10,
        pageIndex: this.pageIndex,
        search: this.searchQuery,
        currentIndex: 0,
      })
    );
    this.assets$ = this.store.select<any[]>(assetsQuery.getAllAssets);
    this.selectedAssets$ = this.store.select<any[]>(
      assetsQuery.getAllSelectedAssets
    );
    this.addSelectedAssetsResponse$ = this.store.select<any[]>(
      assetsQuery.getAddSelectedAssetsResponse
    );

    this.getDataListResposne();
    this.getSelectedAssests();
    this.addSelectedAssetsResponse();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  pageChanged(event) {
    this.length = 0;
    this.selection.clear();
    this.cacheIndex = event.pageIndex;
    this.store.dispatch(
      new LoadAssets({
        pageSize: event.pageSize,
        pageIndex: event.pageIndex + 1,
        currentIndex: event.pageIndex,
        search: this.searchQuery,
      })
    );
  }

  selectAll() {
    this.isSelectAll = !this.isSelectAll;
  }

  clearAll() {
    this.isSelectAll = !this.isSelectAll;
    this.selection.clear();
    this.allSelectedIdList.clear();
  }

  addSelectedDataTableList() {
    this.isLoading = true;
    let params = {
      isAllSelected: this.isSelectAll,
    };
    if (this.isSelectAll) {
      params['search'] = this.searchQuery;
    } else {
      params['ids'] = this.allSelectedIdList.selected;
    }
    this.store.dispatch(new AddSelectedAssets(params));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.searchQuery = filterValue.toLowerCase();
    this.length = 0;
    this.selection.clear();
    this.isSelectAll = false;
    this.cacheIndex = 0;
    this.store.dispatch(
      new LoadAssets({
        pageSize: 10,
        pageIndex: 1,
        search: this.searchQuery,
        currentPageIndex: 0,
      })
    );
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  checkSelection(row) {
    return this.selection.isSelected(row._id);
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.dataSource.data.forEach((data) => {
      if (!this.selection.isSelected(data._id)) {
        this.selection.toggle(data._id);
        this.allSelectedIdList.toggle(data._id);
      }
    });
  }

  checkBoxChangEvent(id) {
    this.selection.toggle(id);
    this.allSelectedIdList.toggle(id);
    this.isSelectAll = false;
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row._id) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }

  showMessage(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  addSelectedAssetsResponse() {
    this.addSelectedAssetsResponse$.subscribe((data) => {
      if (data) {
        this.allSelectedIdList.clear();
        this.allSelectedIdList.select(...data.data[0].selection);
        this.showMessage('Selection updated successfully', 'Done');
        this.isLoading = false;
      }
    });
  }

  getSelectedAssests() {
    this.selectedAssets$.subscribe((data) => {
      if (data) {
        if (data.data.length > 0) {
          this.allSelectedIdList.select(...data.data[0].selection);
        }
        this.store.dispatch(
          new LoadAssets({
            pageSize: 10,
            pageIndex: this.pageIndex + 1,
            search: this.searchQuery,
            currentIndex: 0,
          })
        );
      }
    });
  }

  getDataListResposne() {
    this.assets$.subscribe((data) => {
      if (data && data.total) {
        this.length = data.total;
        this.pageIndex = this.cacheIndex;
        this.paginator.length = data.total;
        data.data.forEach((data) => {
          if (this.isSelectAll) {
            if (!this.allSelectedIdList.isSelected(data._id)) {
              this.allSelectedIdList.toggle(data._id);
            }
            if (!this.selection.isSelected(data._id)) {
              this.selection.toggle(data._id);
            }
          } else {
            if (
              this.allSelectedIdList.isSelected(data._id) &&
              !this.selection.isSelected(data._id)
            )
              this.selection.toggle(data._id);
          }
        });
        this.dataSource.data = data.data;
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
      }
    });
  }
}
