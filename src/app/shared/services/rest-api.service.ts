import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})

export class RestApiService {

    // Define API
    apiURL = environment.API_URL;

    constructor(private http: HttpClient) { }

    /*========================================
      CRUD Methods for consuming RESTful API
    =========================================*/

    // Http Options
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }

    // HttpClient API post() method => Create employee
    getDataTableList(data): Observable<any> {
        return this.http.get<any>(this.apiURL + '/api/names', {
            params: data
        })
            .pipe(
                retry(1),
                catchError(this.handleError)
            )
    }

    addSelectedDataTableList(data): Observable<any> {
        return this.http.put<any>(this.apiURL + '/api/selection', data)
            .pipe(
                retry(1),
                catchError(this.handleError)
            )
    }

    getSelectedDataTableList(): Observable<any> {
        return this.http.get<any>(this.apiURL + '/api/selection')
            .pipe(
                retry(1),
                catchError(this.handleError)
            )
    };

    // Error handling 
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
    }

}